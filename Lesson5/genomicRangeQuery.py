"""
+ First attempt score: 66% | Correctness: 100% | Performance 0%
What failed? added an unecessary loop to map the nucleotides to the given integers
Corrected: Removed that loop and added if-else logic
+ Second attempt score 100%
"""

def solution(S,P,Q):
    impact = {"A":1,"C":2,"G":3,"T":4}
    M = len(P)
    solution = []
    # numS = [impact[x] for x in S] This failed

    # Case 1: S contains one character
    if len(S) == 1:
        return [impact[S]]*M

    # Case 2: General Case
    for i in range(M):
        p = P[i]
        q = Q[i]
        if p == q:
            solution.append(impact[S[p]])
        else:
            # This was added on the second attempt
            subs = S[p:q+1]
            if "A" in subs:
                x = 1
            elif "C" in subs:
                x = 2
            elif "G" in subs:
                x = 3
            else:
                x = 4
            solution.append(x)
    return solution

def main():
    from time import time
    cases = [
            ("CAGCCTA",[2,5,0],[4,5,6])
            ]

    for case in cases:
        S,P,Q = case
        start = time()
        print(solution(S,P,Q))
        print("Execution Time: %s"%(time() - start))

if __name__ == "__main__":
    main()

