"""
First Attempt Score: 75%
What happened? R= Forgot to take into account the extreme case A = 0 duh!
"""

def solution(A,B,K):
    """
    Return the number of integers that are divisible by K, and are between A and B
    """
    ## Case 1: A = B. Only returns 1 if A % K = 0
    if A == B:
        if A % K == 0:
            return 1
        else:
            return 0

    ## Case 2: A < B
    # Case 2.1: K == 1, then return the number of items in the array
    if K == 1:
        return B - A + 1
    # Case 2.2: B < K, if K is greater, there is no way that B % K = 0
    elif B < K:
        return 0
    # Case 2.3: B = K, if B and K are equal then B % K = 1 and therefore just
    #           one elmement of the array met the condition
    elif B == K:
        if A == 0:
            return 2
        else:
            return 1
    # Case 2.4: A <= K, since K is greater or equal than A, there is no sense in looping over the first elements of the array before K
    elif A <= K and A != 0:
        return moduleInts(K,B,K)
    else:
        return moduleInts(A,B,K)

def moduleInts(p,q,k):
    """
    Return the number of integers divided by k between the range p, q
    """
    if p == k:
        return int(q/k)
    elif p == 0:
        return int(q/k) + 1
    elif p % k == 0 and p != k:
        return int(q/k) - int(p/k) + 1
    else:
        return int(q/k) - int(p/k)

def main():
    from time import time

    cases = [
            (6,13,3),
            (6,11,2),
            (5,10,3),
            (5,10,4),
            (4,10,4),
            (4,10,3),
            (0,14,2),
            (0,200000,200000)
            ]

    for i in cases:
        A,B,K = i
        start = time()
        print("Test Case: ",i)
        print(solution(A,B,K))
        print("Execution time: %s\n"%(time()-start))

if __name__ == "__main__":
    main()
