"""
Array A contains only 0s and/or 1s
- 0 represents a car traveling east,
- 1 represents a car traveling west

Goal: Count passing Cars.
"""

def solution(A):
    carSum = 0
    N = len(A)
    zeros = 0
    for i in range(N):
        if A[i] == 0:
            zeros += 1
        else:
            carSum += zeros
        if carSum > 1000000000:
            return -1
    return carSum

def main():
    from time import time
    cases = [
            [0,1,0,1,1]
            ]
    for A in cases:
        start = time()
        print(solution(A))
        print("Execution Time: %s"%(time() - start))

if __name__ == "__main__":
    main()


