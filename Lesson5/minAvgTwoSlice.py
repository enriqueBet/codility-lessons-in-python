"""
First attempt score: 
"""

def solution(A):
    N = len(A)
    ## Case 1: The array A only contains 2 elements
    if N == 2:
        return 0

    ## Case 2: General case
    # Get the accumulative sum
    P = prefix_sums(A)

    i = 0
    j = 1
    idx = 0
    while i + j <= 2*N - 3:
        n = j - i + 1
        avgS = count_total(P,i,j)/n
        # print(avgS, "i=%s , j=%s"%(i,j))
        if i + j == 1:
            avg = avgS
        if avgS < avg and i + j != 1:
            avg = avgS
            idx = i
        j += 1
        if j > N-1:
            i += 1
            j = i + 1
            sumArray = A[i]

    return idx

def prefix_sums(A):
    n = len(A)
    P = [0]*(n+1)
    for k in range(1,n + 1):
        P[k] = P[k - 1] + A[k - 1]
    return P

def count_total(P,x,y):
    return P[y + 1] - P[x]

def main():
    from time import time
    cases = [
            [4,2,2,5,1,5,8],
            [5, 6, 3, 4, 9],
            [-3, -5, -8, -4, -10]
            ]

    for A in cases:
        start = time()
        print(solution(A))
        print(A)
        print(prefix_sums(A))
        print("Execution time: %s"%(time() - start))

if __name__ == "__main__":
    main()

