"""
First approach score:
"""

def solution(A):
    N = len(A)

    # Case 1: No items inside the array, or one circle
    if N <= 1:
        return 0

    # Case 2: Find the numner of open points and closing points
    open_points = []
    closing_points = []
    number_of_open_circles = 0
    for i in range(N):
        open_points.append(i - A[i])
        closing_points.append(i + A[i])

    open_points.sort()
    closing_points.sort()

    # Case 2: General case
    intersections = 0
    start = 0
    for i in range(len(closing_points)):
        while start < len(open_points) and open_points[start] <= closing_points[i]:
            start +=1
        intersections += start - i - 1
        if intersections > 10000000:
            return -1
    return intersections

if __name__ == "__main__":
    from time import time
    cases = [
            [1,5,2,1,4,0],
            [1,1,1]
            ]
    for A in cases:
        start_time = time()
        print(f"Number of Intersections: {solution(A)}")
        print(f"Execution Time: {time() - start_time}")

