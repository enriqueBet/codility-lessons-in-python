"""
First Approach Score: 87%
What happen? Forgot to take into account the min number of elements after filtering
Second Approach Score: 100%
"""

def solution(A):
    # Calculate the length
    N = len(A)
    # Case 1: The array does not contain enough elements
    if N < 3:
        return 0

    A.sort()
    # Case 2: Check if all the numbers are negatives, in that case is not possible
    #         to create triangular set
    if A[-1] <= 0:
        return 0

    # Filter negative and zero numbers
    A = [a for a in A if a > 0]
    N = len(A)

    # Case 1: The array does not contain enough elements
    if N < 3:
        return 0

    # Case 3: General Case, for positive numbers only.
    for i in range(1,N-1):
        p = A[i-1]
        c = A[i]
        n = A[i+1]

        if p + c > n and c + n > p and n + p > c:
            return 1
    return 0

def main():
    from time import time

    cases = [
            [10,2,5,1,8,20],
            [10,50,5,1],
            [0, 2147483647, 2147483647],
            []
            ]

    for A in cases:
        start = time()
        print(solution(A))
        print("Execution Time: ",time()-start)

if __name__ == "__main__":
    main()
