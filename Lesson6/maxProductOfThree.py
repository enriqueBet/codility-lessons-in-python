"""
First Approach Score: 44% :(
+ What happened? This test case didn't work for obivous reasons
A = [-5, 5, -5, 4]
then after sorting we have:
    A = [-5,-5,4,5]
    so alpha = 100
    and beta = -100
when is easy to see that there is a better triplet:
    gamma = 125
so the fix that I am adding here is two combinations
    gamma = A[-1]*A[-2]*A[0]
    delta = A[-1]*A[0]*A[1]
Second Approach Score: 100% yay! so that did the work
"""

def solution(A):
    """
    The approach pictured on my notes (check the directory 'Notes'). was to sort the array and then since A is between (-1000 to 1000), there are some possibilities:
    Pos 1: The array contains only negative numbers
    Pos 2: The array only contains positive numbers
    Pos 3: The array is full of zeros
    Pos 4: A mix of all of them

    I belive the correct approach is to sort the array A and take the first three and the last three elements, perform the product and compare the results to see which is gerater than the other.
    """
    # Get the length of the Array
    N = len(A)

    if N == 3:
        return A[0]*A[1]*A[2]
    else:
        # Sort the array
        A.sort()
        alpha = A[-1]*A[-2]*A[-3]
        beta = A[0]*A[1]*A[2]
        gamma = A[-1]*A[0]*A[1]
        delta = A[-1]*A[-2]*A[0]
        return max([alpha,beta,gamma,delta])

def main():
    from time import time
    cases = [
            [-3,1,2,-2,5,6],
            [1,2,3],
            [-1,-2,-3],
            [-1,0,0,0,0,0,0,0,0,0,0,0,1],
            [-5,5,-5,4]
            ]
    for A in cases:
        start = time()
        print(solution(A))
        print("Execution time: %s"%(time() - start))

if __name__ == "__main__":
    main()

