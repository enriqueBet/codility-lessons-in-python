"""
First Attempt Score: 100% yay!!
"""
def solution(A):
    """
    Returns the number of distinct values in Array A
    Constrains:
    N integer within the range[0,100000]
    A integer within the range[-1000000,10000000]
    """
    N = len(A)
    ## Case 1: N = 0, in this case the array is empty and thus the number of
    #          disctinct values is zero
    if N == 0:
        return 0

    ## Case 2: N = 1, thus the value  is unique
    if N == 1:
        return 1

    ## Case 3: N > 1, in this case the values need to be counted with the help of a
    #          hash table
    numDict = {}
    count =  0
    for i in range(N):
        try:
            numDict[A[i]]
        except:
            numDict[A[i]] = 1
            count += 1
    return count

def main():
    from time import time

    cases = [
            [2,1,1,2,3,1],
            [],
            [1],
            ]

    for A in cases:
        start = time()
        print("Test Case: ",A)
        print(solution(A))
        print("Execution time: %s\n"%(time() - start))

if __name__ == "__main__":
    main()



