def solution(A):
    # write your code in Python 3.6
    N = len(A)
    # A.sort()
    if N == 0:
        return 1
    elif N == 1:
        if A[0] == 1:
            return 2
        else:
            return 1
    else:
        sumN = N*(N+1)/2
        sumA = sum(A)
        delta = sumA - sumN
        if delta > 0:
            return int(N - delta) + 1
        else:
            return N + 1

def main():
    cases = [
            [2,3,1,5]
            ]
    for A in cases:
        print(solution(A))

if __name__ == "__main__":
    main()
