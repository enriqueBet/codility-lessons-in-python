def solution(X,Y,D):
    if X == Y:
        return 0
    jumps = int((Y - X)/D)
    quant = X + D*jumps
    if quant < Y:
        jumps += 1
    return jumps

def main():
    X = 10
    Y = 85
    D = 30
    print(solution(X,Y,D))

if __name__ == "__main__":
    main()
