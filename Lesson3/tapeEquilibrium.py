def solution(A):
    N = len(A)
    if N == 2:
        if A[0] >= A[1]:
            return A[0] - A[1]
        else:
            return A[1] - A[0]
    if len(set(A)) == 1:
        if N % 2 == 0:
            return 0
        else:
            return A[0]

    sum1 = A[0]
    sum2 = sum(A[1:])
    buffDiff = abs(sum1 - sum2)
    for i in range(1,N-1):
        sum1 += A[i]
        sum2 -= A[i]
        diff = abs(sum1 - sum2)
        if diff < buffDiff:
            buffDiff = diff
    return buffDiff


def main():
    A = [3,1,2,4,3]
    print(solution(A))

if __name__ == "__main__":
    main()



