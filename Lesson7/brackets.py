"""
First Approach Score: 87%
What happened?

1.904 s
TIMEOUT ERROR, running time: 1.904 sec., time limit: 0.336 sec.
stderr:

Traceback (most recent call last):
  File "exec.py", line 123, in <module>
    main()
  File "exec.py", line 85, in main
    result = solution( S )
  File "/tmp/solution.py", line 14, in solution
    return solution(K)
  File "/tmp/solution.py", line 14, in solution
    return solution(K)
  File "/tmp/solution.py", line 14, in solution
    return solution(K)
  [Previous line repeated 993 more times]
  File "/tmp/solution.py", line 5, in solution
    if S == "":
RecursionError: maximum recursion depth exceeded in comparison

Actions: Removing the Case S == "" from the begining
"""

"""
A string S consisting of N characters is considered to be properly nested if any
of the following conditions is true:
    1. S is empty
    2. S has the form "(U)" or "[U]" or "{U}" where U is a properly nested string
    3. S has the form "VW" where V and W are properly neseted strings.

example:
    the string "{[()()]}" is properly nested but "([)()]" is not
"""

def solution(S):
    N = len(S)

    if N == 0:
        return 1

    if N % 2 !=0:
        return 0

    K = S.replace("{}","").replace("()","").replace("[]","")
    M = len(K)
    if M != 0:
        if M != N:
            return solution(K)
        else:
            return 0
    else:
        return 1

if __name__ == "__main__":
    from time import time

    cases = [
            "{[()()]}",
            "([)()]",
            "("*100000 + ")"*100000 + ")("
            ]

    for S in cases:
        start = time()
        print(solution(S))
        print(f"Execution Time: {time() - start}")

