"""
You are given two non-empty arrays A and B consisting of N integers. Arrays A
and B represent N voracious fish in a river, ordered downstream along the 
flow of the river.

The fish are numbered from 0 to N-1. If P and Q are two fish and P < Q, then
fish P is initially upstream of fish Q. Initiallym, each fish has a unique 
position.

Fish number P is represented by A[P] and B[P]. Array A contains the sizes of
the fish. All its elements are unique. Array B contains the directions of the
fish. It contains only 0s and/or 1s, where:
    - 0 represents a fish flowing upstream,
    - 1 represents a fish flowing downstream.

If two fish move in opposite directions and there are no other (living) fish between them, they will eventually meet each other. Then only one fish can stay alive - the larger fish eats the smaller one. More precisely, we say that two fish P and
Q meet each other when P < Q, B[P] = 1 and B[Q] = 0, and there are no living 
fish between them. After they meet:
    - If A[P] > A[Q] then P eats Q, and P will still be flowing downstream,
    - If A[Q] > A[P] then Q eats P, and Q will still be flowing upstream.
``
We assume that all the fish are flowing at the same speed. That is, fish moving
in the same direction never meet. The goal is to calculate the number of fish that
will stay alive.
"""

def solution(A,B):
    # Get the length of the queues
    N = len(A)

    # Case 1: In case that there is only one fish
    if N == 1:
        return 1

    # Case 2: General Case

    # Get in a list the fishes going upstream and downstream
    fish_up = []
    fish_down = []
    for i in range(N):
        if B[i] == 1:
            fish_down.append(A[i])
        else:
            fish_up.append(A[i])

    if len(fish_down) == 0 or len(fish_up) == 0:
        return N

    # Check number of alive fishes
    survivors = []
    for i in range(N):
        while survivors and B[survivors[-1]] and B[i] == 0:
            print(survivors)
            survivor = survivors.pop()
            print(survivors)
            if A[survivor] > A[i]:
                i = survivor
        survivors.append(i)
    return len(survivors)


if __name__ == "__main__":
    from time import time
    cases = [
            ([4,3,2,1,5],[0,1,0,0,0]),
            ([4,3,2,1,5],[0,1,1,1,1]),
            ([1],[0]),
            ([1],[1]),
            ([4,3,2,1,5],[1,1,1,1,1]),
            ([4,3,2,1,5],[0,0,0,0,0]),
            ]
    for case in cases:
        A,B = case
        start = time()
        print(f"\nCase: A {A} B {B}")
        print(f"Alive {solution(A,B)}")
        print(f"Execution time: {time() - start}\n")
