"""
First Attempt: 75% > Correctness 100%, Performance 50% | Approach replacein '()'
Second Attempe: 100% | Added stack/pop approach
"""

def solution(S):
    """
    Constrains:
        N : [0,1000000]
        S consists only of characters "(" and/or ")"
    """
    # Get the length of the array
    N = len(S)

    # Case 1: The string is formed by 1 element or no elements
    if N % 2 == 1:
        return 0

    if N == 0:
        return 1
    ://app.codility.com/programmers/lse: pass

    # Case 2: General case
    stack = []
    for k in S:
        if k == "(":
            stack.append(k)
        elif k == ")" and stack:
            stack.pop()
        else:
            return 0

    if len(stack) > 0:
        return 0
    else:
        return 1

if __name__ == "__main__":
    from time import time
    cases = [
            "(()(())())",
            "())"
            ]

    for S in cases:
        start_time = time()
        print(f"Solution: {solution(S)}")
        print(f"Execution time: {time()-start_time}")
