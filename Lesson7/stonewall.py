def solution(H):
    """
    Constraints:
        N : [1,100000]
        H : [1,1000000000]
    """
    # Get the length of the array
    N = len(H)

    # Case 1:
    if N == 1:
        return 1

    # Case 2:
    blocks = []
    cubes = 0
    for stone in H:
        while blocks and blocks[-1] > stone:
            print(blocks)
            blocks.pop()
        if blocks and blocks[-1] == stone:
            continue
        else:
            blocks.append(stone)
            cubes += 1
    return cubes

if __name__ == "__main__":
    from time import time
    cases = [
            [8,8,5,7,9,8,7,4,8]
            ]
    for H in cases:
        start_time = time()
        print(f"Number of blocks: {solution(H)}")
        print(f"Execution time: {time() - start_time}")



