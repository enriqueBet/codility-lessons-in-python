def solution(A):
    if len(A) == 1:
        return A[0]

    # init dir
    occurrence = {}

    for a in A:
        try:
            del occurrence[a]
        except:
            occurrence[a] = 1

    odd = list(occurrence.keys()[0])
    return odd

def main():
    A = [9,3,9,3,9,7,9]

if __name__ == "__main__":
    main()
