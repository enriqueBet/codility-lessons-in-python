def solution(A, K):
    # Case 1: If the numbers are equal or the array is empty return nothing
    if len(set(A)) <= 1:
        return A
    # Case 2: If the rotation is the same length as the array
    M = len(A)
    if M == K:
        return A
    # Case 3: The asked rotations are higher than the array length
    if K >= M:
        delta = K%M
    # Case 4: Rotations are smaller than the array length
    else:
        delta = K

    print(delta)
    print(A)
    print(M)
    A = arrayRotationMethod(A,delta,M)

    return A

def arrayRotationMethod(A,delta,M):
    # Initalization vars
    B = A.copy()
    for i in range(M):
        if i + delta >= M:
            idx = i + delta - M
        else:
            idx = i + delta
        B[idx] = A[i]

    return B

def main():
    A = [1,1,2,3,5]
    K = int(input())
    print(solution(A,K))

    # temp1 = A[0]
    # c = 0
    # idx = 0
    # # Start the rotations
    # while c != M:
    #     idx += delta
    #     if idx >= M:
    #         idx -= M
    #     temp2 = A[idx]
    #     A[idx] = temp1
    #     temp1 = temp2
    #     if M % 2 == 0:
    #         idx += 1
    #     c += 1
    # return A

if __name__ == "__main__":
    main()
