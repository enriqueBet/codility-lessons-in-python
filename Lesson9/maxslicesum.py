def solution(A):
    N = len(A)

    if N == 1:
        return A[0]

    infimum = -2147483648
    max_slice = max_value = 0
    max_number = infimum
    for a in A:
        max_value = max(0,max_value + a)
        max_slice = max(max_value,max_slice)
        if a > max_number:
            max_number = a

    if max_slice == 0:
        return max_number
    else:
        return max_slice

if __name__ == "__main__":
    from time import time
    cases = [
            [3,2,-6,4,0],
            [-1,-2,-3,8,-9],
            [-1,-3,-4,-5,-6]
            ]

    for A in cases:
        start_time = time()
        print(f"Solution: {solution(A)}")
        print(f"Execution time: {time() - start_time}")

