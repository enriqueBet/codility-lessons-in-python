def solution(A):
    N = len(A)

    if N == 0:
        return 0

    if N == 1:
        return 0

    min_value = A[0]
    max_returns = 0
    for i in range(1,N):
        a = A[i]
        if a < min_value:
            min_value = a
            continue
        else:
            if a - min_value > max_returns:
                max_returns = a - min_value

    return max_returns

if __name__ == "__main__":
    from time import time
    cases = [
            [23171,21011,21123,21366,21013,21367],
            [300,299,288],
            [200,200,200,200]
            ]

    for A in cases:
        start_time = time()
        print(f"Solution: {solution(A)}")
        print(f"Execution time: {time() - start_time}")

