def solution(A):
    N = len(A)

    # Case 1:
    if N == 3:
        return 0

    # Case 2:
    # We will keep track of the minimum number inside a two slices sum
    max_ending = 0
    min_value = None
    max_slice = 0
    max_idx = 0
    min_idx = 0

    # Find the max slice sum
    for i in range(1,N-1):
        a = A[i]
        # Initial values
        if min_value is None:
            min_value = a

        # Find the min value
        if a < min_value:
            min_value = a
            min_index = i

        max_ending = max(0,max_ending + a)

        if max_slice < max_ending:
            max_slice = max_ending
            max_idx = i

    if max_slice == 0:
        return 0

    if min_value < 0:
        return max_slice

    return max_slice - min_value

if __name__ == "__main__":
    from time import time

    cases = [
            [3,2,6,-1,4,5,-1,2],
            [0,10,-5,-2,0]
            ]

    for A in cases:
        start_time = time()
        print(f"Solution: {solution(A)}")
        print(f"Execution time: {time()-start_time}")
