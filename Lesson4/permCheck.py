def solution(A):
    """
    A permutation is a sequence contaning each element from 1 to N once, and only once
    """

    ## Case 1: Single Value list
    N = len(A)
    if N == 1:
        if A[0] == 1:
            return 1
        else:
            return 0

    # Sort the element for the incoming cases
    A.sort()
    X = A[-1]
    ## Case 2: List with only one element
    # Sort A to obtain the last element for the list and calculate the Gauss summation formula
    if A[0] == A[-1]:
        return 0

    ## Case 3: The elements on the extreme doesn't match with the values that should have
    if A[0] != 1 or X != N:
        return 0

    ## Case 4: Other cases
    sumX = X*(X + 1)/2
    sumA = 0
    aDict = {}

    # Loop over the elemns of the list to sum only once this elements
    for i in range(N):
        try:
            x = aDict[A[i]]
        except:
            aDict[A[i]] = 1
            sumA += A[i]

    # Comparte the summation results to see if is a permutation. If the list is a permutation according to the given definition the sum must match.
    if sumX == sumA:
        return 1
    else:
        return 0

def main():
    cases = [
            [4,1,3,2],
            [9, 5, 7, 3, 2, 7, 3, 1, 10, 8],
            [1,1],
            [1,1,2,3,4,5]
            ]
    for A in cases:
        print(solution(A))

if __name__ == "__main__":
    main()
