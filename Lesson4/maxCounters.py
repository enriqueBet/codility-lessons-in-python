def solution(N,A):
    counters = [0]*N
    maxValue = 0
    for i in range(len(A)):
        a = A[i]
        idx = a - 1
        if 1 <= a <= N:
            counter = counters[idx]
            counters[idx] += 1
            if counter + 1 > maxValue:
                maxValue = counter + 1
        else:
            counters = [maxValue]*N
    return counters

def main():
    from time import time
    cases = [
            (5,[3,4,4,6,1,4,4]),
            ]
    for P in cases:
        N,A = P
        start = time()
        print(solution(N,A))
        print("Execution Time: %s"%(time() - start))

if __name__ == "__main__":
    main()
