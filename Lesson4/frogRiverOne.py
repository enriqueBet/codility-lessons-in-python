def solution(X,A):
    if len(A) == 1:
        if X > A[0] or X < A[0]:
            return -1
        else:
            return 0

    if len(set(A)) == 1:
        return -1
    sumX = X*(X+1)/2
    sumA = 0
    numDict = {}
    for i in range(0,len(A)):
        if A[i] <= X:
            try:
                x = numDict[A[i]]
            except:
                numDict[A[i]] = 1
                sumA += A[i]
        if sumA == sumX:
            return i
    return -1

def main():
    cases = [
            (5,[1,2,3,5,3,1])
            ]
    for i in cases:
        X,A = i
        print(solution(X,A))

if __name__ == "__main__":
    main()

