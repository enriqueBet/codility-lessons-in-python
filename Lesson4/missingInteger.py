"""
Score on first attempt 100% yay!
"""

from time import time

def solution(A):
    A = [a for a in A if a > 0]
    N = len(A)
    A.sort()
    ## Case 1: Single element list
    if N == 1:
        x = A[0]
        if x != 1:
            return 1
        else:
            return 2

    ## Case 2: Negative-element list
    if N == 0:
        return 1

    ## Case 3: Missing 1 on the first element
    if A[0] != 1:
        return 1

    ## Case 4: General case
    delta = 0
    for i in range(0,N-1):
        delta = A[i + 1] - A[i]
        if delta >= 2:
            return A[i] + 1
    return A[-1] + 1

def main():
    cases = [
            [1,3,6,4,1,2],
            [1,2,3],
            [-1,-3]
            ]
    for A in cases:
        start = time()
        print(solution(A))
        print("Execution time: %s seconds"%(time() - start))

if __name__ == "__main__":
    main()
