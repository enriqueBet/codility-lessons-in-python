def solution(A):
    N = len(A)
    size = 0
    candidate_index = 0
    for k in range(N):
        if size == 0:
            size += 1
            value = A[k]
            candidate_index = k
        else:
            if (value != A[k]):
                size -= 1
            else:
                size += 1
    candidate = -1
    if (size > 0):
        candidate = value
    leader = -1
    count = 0
    for k in range(N):
        if (A[k] == candidate):
            count += 1
    if (count > N//2):
        return candidate_index
    else:
        return -1


if __name__ == "__main__":
    from time import time
    cases = [
            [3,4,3,2,3,-1,3,3]
            ]
    for A in cases:
        start_time = time()
        print(f"Solution: {solution(A)}")
        print(f"Execution time: {time()-start_time}")
