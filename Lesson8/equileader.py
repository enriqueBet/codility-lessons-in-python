def find_the_leader(B):
    N = len(B)
    size = 0
    for k in range(N):
        if size == 0:
            value = B[k]
            size += 1
        else:
            if value != B[k]:
                size -= 1
            else:
                size += 1

    candidate = None
    if size > 0:
        candidate = value
    else:
        return None

    count = 0
    for k in range(N):
        if B[k] == candidate:
            count += 1

    if count > N//2:
        return candidate
    else:
        return None

def solution(A):
    N = len(A)

    # Find the leader of the array
    number_of_leaders = 0
    for i in range(N-1):
        leaderA = find_the_leader(A[:i+1])
        leaderB = find_the_leader(A[i+1:])

        if leaderA and leaderB:
            if leaderA == leaderB:
                number_of_leaders += 1
        print(f"Series A: {A[:i+1]}")
        print(f"Leader A: {leaderA}")
        print(f"Series B: {A[i+1:]}")
        print(f"Leader B: {leaderB}")
        print(f"Number of equileaders: {number_of_leaders}")

    return number_of_leaders

if __name__ == "__main__":
    from time import time
    cases = [
            [4,3,4,4,4,2]
            ]
    for A in cases:
        start_time = time()
        print(f"Solution: {solution(A)}")
        print(f"Execution Time: {time() - start_time}")
